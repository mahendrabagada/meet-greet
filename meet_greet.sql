-- phpMyAdmin SQL Dump
-- version 4.5.3.1deb1.trusty~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2016 at 09:27 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meet&greet`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `username`, `password`) VALUES
(1, 'mahendra', 'mahendra@007'),
(2, 'admin', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `card_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL DEFAULT '0',
  `card_thumb` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `card_back` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `card_fore` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `card_font_color` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`card_id`, `c_id`, `card_thumb`, `card_back`, `card_fore`, `card_font_color`) VALUES
(47, 3, '1008-21 (9).gif', '1008-21 (10).gif', '1008-21 (11).gif', 'red'),
(18, 3, '1008-21 (12).gif', '1008-21 (13).gif', '1008-21 (14).gif', 'SKY'),
(16, 3, '1008-21 (6).gif', '1008-21 (7).gif', '1008-21 (8).gif', 'red'),
(13, 3, '1008-021-1.gif', '1008-021_bg.gif', '1008-021.gif', 'green'),
(20, 5, '1036-010-34-10681.gif', '1036-010-34-1068_bg.gif', '1036-010-34-1068.gif', 'pink'),
(8, 3, '1008-21 (15).gif', '1008-21 (16).gif', '1008-21 (17).gif', 'pink'),
(23, 3, '1008-21 (3).gif', '1008-21 (4).gif', '1008-21 (5).gif', 'red'),
(24, 5, '8500-031-1.gif', '8500-031_bg.gif', '8500-031.gif', 'green'),
(25, 5, '8566-040-1.gif', '8566-040_bg.gif', '8566-040.gif', 'pink'),
(26, 8, '2232-046-1.gif', '2232-046_bg.gif', '2232-046.gif', 'red'),
(27, 8, '2232-041-1.gif', '2232-041_bg.gif', '2232-041.gif', 'green'),
(28, 8, '2232-042-1.gif', '2232-042_bg.gif', '2232-042.gif', 'red'),
(52, 3, 'hbd.jpg', 'hbd 2.jpg', 'hbd 3.jpg', 'blue'),
(30, 6, '1036-015-14-10681.gif', '1036-015-14-1068_bg.gif', '1036-015-14-1068.gif', 'green'),
(31, 6, '1036-015-23-1069.gif', '1036-015-23-1069_bg.gif', '1036-015-23-10691.gif', 'red'),
(32, 6, '1036-017-1.gif', '1036-017_bg.gif', '1036-017.gif', 'green'),
(33, 6, '1036-018-1.gif', '1036-018_bg.gif', '1036-018.gif', 'red'),
(34, 5, '8500-028-1.gif', '8500-028_bg.gif', '8500-028.gif', 'green'),
(35, 4, '1012-003-27-1036.gif', '1012-003-27-1036_bg.gif', '1012-003-27-10361.gif', 'red'),
(36, 4, '1012-003-31-1028.gif', '1012-003-31-1028_bg.gif', '1012-003-31-10281.gif', 'green'),
(37, 4, '1012-003-61-1068.gif', '1012-003-61-1068_bg.gif', '1012-003-61-10681.gif', 'red'),
(41, 1, '1065-035-1.gif', '1065-035_bg.gif', '1065-035.gif', 'green'),
(40, 1, '1065-034-1.gif', '1065-034_bg.gif', '1065-034.gif', 'red'),
(42, 2, 'thumb3.png', 'back_3.gif', 'fore_3.JPG', 'pink'),
(43, 2, 'thumb_2.png', 'back_2.gif', 'for_2.jpg', 'red'),
(44, 2, 'thumb_1.png', 'back_1.gif', 'for_1.jpg', 'green'),
(45, 2, 'thumb_4.png', 'back_4.gif', 'fore_4.JPG', 'red'),
(53, 5, 'miss.jpeg', 'mmmmm.jpg', 'good-morning.jpg', 'blue'),
(54, 9, 'download.png', 'download2.png', 'download 1.png', 'blue'),
(56, 9, 'download.png', 'download2.png', 'download 1.png', 'red');

-- --------------------------------------------------------

--
-- Table structure for table `card_category`
--

CREATE TABLE `card_category` (
  `c_id` int(11) NOT NULL,
  `c_category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_category`
--

INSERT INTO `card_category` (`c_id`, `c_category`) VALUES
(1, 'Family'),
(2, 'Friends'),
(3, 'Birthday'),
(4, 'Sorry'),
(5, 'Miss You'),
(6, 'Love You'),
(8, 'Thank You'),
(9, 'valentain');

-- --------------------------------------------------------

--
-- Table structure for table `query_bank`
--

CREATE TABLE `query_bank` (
  `query_id` int(11) NOT NULL,
  `query_type` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `email_id` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `message` text COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `query_bank`
--

INSERT INTO `query_bank` (`query_id`, `query_type`, `email_id`, `message`) VALUES
(1, 'xyz@gmail.com', 'Query', 'aaaaa'),
(2, '', 'Select Type', ''),
(3, '', 'Select Type', ''),
(4, '', 'Select Type', ''),
(5, '', 'Select Type', ''),
(6, '', 'Select Type', ''),
(7, '', 'Select Type', ''),
(8, '', 'Select Type', ''),
(9, 'Compliment', 'xyz@gmail.com', 'You can drop any INDEX by using DROP clause along with ALTER command. Try out the following example to drop above-created index.'),
(10, 'Select Type', '', ''),
(11, 'Select Type', '', ''),
(12, 'Compliment', 'abc@gmail.com', 'etrgdgedrtgdertgdfgfgfgg');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `userid` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `repassword` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`userid`, `password`, `repassword`, `name`, `gender`, `date`, `city`, `state`, `email`) VALUES
(1, '123456', '123456', 'mahendra', 'Male', '1995-01-01', 'rjkt', 'rajsthan', 'GHJK'),
(4, 'mahimuskan', 'mahimuskan', 'bagada mahi', 'male', '1995-05-14', 'delhi', 'panjab', 'bagada@gmail.com'),
(5, '143143', '143143', 'xyz', 'female', '1992-02-29', 'ranchi', 'tamilnadu', 'xyz@gmail.com'),
(6, 'abcabc', 'abcabc', 'abc ', 'Male', '0000-00-00', 'ahmedabad', 'rajsthan', 'abc@gmail.com'),
(7, 'tofik123', 'tofik123', 'tofik', 'Male', '1995-02-25', 'delhi', 'madhya pradesh', 'tofik@yahoo.com'),
(8, '', '', '', 'Select Gender', '0000-00-00', '', '', ''),
(9, '', '', '', 'Select Gender', '0000-00-00', '', '', ''),
(10, '', '', '', 'Select Gender', '0000-00-00', '', '', ''),
(11, '123456', '123456', 'er', 'Male', '1995-03-23', 'rajkot', 'gujrat', 'er@gmail.com'),
(12, '987654', '987654', 'bagda', 'Male', '1985-02-23', 'jaypur', 'rajsthan', 'bagada@gmail.com'),
(13, '987654', '987654', 'xyz', 'Male', '1985-02-23', 'jaypur', 'rajsthan', 'xyz@gmail.com'),
(14, 'kaprat', 'kaprat', 'kaprat', 'Male', '1985-02-23', 'delhi', 'madhy pradesh', 'kaprat@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `send_cards`
--

CREATE TABLE `send_cards` (
  `card_key` bigint(20) NOT NULL,
  `card_id` int(11) NOT NULL,
  `sender_name` varchar(100) NOT NULL,
  `receiver_name` varchar(100) NOT NULL,
  `sender_email_id` varchar(100) NOT NULL,
  `receiver_email_id` varchar(100) NOT NULL,
  `header` varchar(100) NOT NULL,
  `footer` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `send_cards`
--

INSERT INTO `send_cards` (`card_key`, `card_id`, `sender_name`, `receiver_name`, `sender_email_id`, `receiver_email_id`, `header`, `footer`, `message`) VALUES
(1, 13, 'Nirav', 'Mahendra', 'niravbhanvadiya@gmail.com', 'bagadamahendra@gmail.com', 'Happy birthday', 'SeE YoU BuY BuY', 'many many happy return of the day \r\nHappy birthday dear...'),
(2, 27, 'dipak', 'nirav', 'rathoddipak@gmail.com', 'niravbhanavadiya@gmail.com', 'Thank You', 'bUy BuY', 'Thank you my friend....'),
(3, 30, 'nitin', 'PAYAL', 'nitinkhant@gmail.com', 'payalpatel2gmail.com', 'I LOVE YOU', 'BUY BUY', 'HI PAYAL\r\nI LoVe YoU......'),
(4, 19, 'fgbxh', 'cxgv', 'cvbc', 'cfxhbg', 'gfbxf', 'fhxfg', 'fhbxg'),
(5, 20, 'Kano', 'jay', 'kano5252552@gmail.com', 'jay007@gmail.com', 'hellooooo....', 'gooood buuuy', 'hi \r\nhow are you?\r\n'),
(6, 36, 'mahi', 'nirav', 'bagadamahi@gmail.com', 'niravbhanavadiya@gmail.com', 'Ilove you', 'dfgh', 'mmmmmmmmmmmmmiiiiiiiii\r\nlllllllllll\r\nkkkkkkkkk'),
(7, 24, 'dddddd', 'ddddd', 'ghjj', 'ccdfg', 'fghhdd', 'dvbhh', 'dddddddddddddddddd'),
(8, 24, 'yyyyyyyy', 'qweddd', 'sdff', 'ssfdff', 'sdc', 'asd', 'sdcssdrrrr'),
(9, 20, 'Shailesh', 'J Chawda', 'kano5252552@gmail.com', 'kishorchawda@gmail.com', 'This is DEmo', 'Shailesh J Chawda', 'This is demo'),
(10, 32, 'bbbbbbbbbbb', 'ssssssss', 'ssssssss', 'ddddddddddddd', 'tttttttttttttt', 'eeeeeeeeeeeeee', 'mnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmn'),
(11, 25, 'ddddddddd', 'ssssssssss', 'iuytre', 'bvcxz', 'bvcxz', 'mnbvc', 'jhgfdxz'),
(12, 36, 'nilesh', 'gaurav', 'nilesu@gmail.com', 'makwanagaurav@gmail.com', 'hiiiiiiiiiiiii', 'i miss u', 'u r db hu iwhgdb sj fgt njmj dhjk'),
(13, 20, 'nirav', 'nitin', 'nirav@gmail.com', 'nitin@gmail.com', 'hello!!!!', 'by by', 'hello\r\nhow are you?'),
(14, 43, '', '', '', '', '', '', ''),
(15, 43, '', '', '', '', '', '', ''),
(16, 43, '', '', '', '', '', '', ''),
(17, 31, '', '', '', '', '', '', ''),
(18, 43, '', '', '', '', '', '', ''),
(19, 36, 'sdfghj', 'wertyu', 'zxcvbn', 'sdfghjk', 'cvbnmfg', 'hjkl', 'fghjkl'),
(20, 36, 'rtyuio', 'dfghjkl', 'cvbnm,', 'tghyujikol;', 'vbnm,', 'fghjk', 'fghjkl'),
(21, 36, '', '', '', '', '', '', ''),
(22, 36, '', '', '', '', '', '', ''),
(23, 24, '', '', '', '', '', '', ''),
(24, 40, '', '', '', '', '', '', ''),
(25, 43, '', '', '', '', '', '', ''),
(26, 36, '', '', '', '', '', '', ''),
(27, 40, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'cvbnm,.'),
(28, 43, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'jijij'),
(29, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'dsdsd'),
(30, 36, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'dfdfdfdfdf'),
(31, 43, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'ASAASAS'),
(32, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'dsdsd'),
(33, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'weewee'),
(34, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'weewee'),
(35, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', 'weewee'),
(36, 18, 'fff', 'nkt', 'bagada@yahoo.in', 'nkt@yahoo.com', 'ghiiii', 'giiiiiiii', '6i7gftyi'),
(37, 0, 'fff', 'nkt', '', '', '', '', ''),
(38, 0, 'fff', 'nkt', '', '', '', '', ''),
(39, 42, 'mahi', 'nkt', 'er@yahoo.in', 'nilam@gmail.com', 'hiiiiiii', 'how r u?', 'why r u not talk to me'),
(40, 43, 'mahi', 'abc', 'bagada@yahoo.in', 'abc@yahoo.com', 'hiiiiiiii', 'how r u?', 'hello'),
(41, 43, 'mahi', 'abc', 'bagada@yahoo.in', 'abc@yahoo.com', 'hiiiiiiii', 'how r u?', 'hello'),
(42, 18, '', '', '', '', '', '', ''),
(43, 18, 'maja', 'jbkjbkj', 'uhjb', 'ubhbb', 'hbnbnmb', 'oiujhjjk', 'wefghjrtcfyvgubhnj'),
(44, 24, '', '', '', '', '', '', ''),
(45, 8, '', '', '', '', '', '', ''),
(46, 24, 'mahendra', 'bagada', 'kaprat@gmail.com', 'bagada@gmail.com', 'ighhkhkh', 'hhhkhkhkh', 'hkjhkjhjkhkjhkjhkjiyg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `card_category`
--
ALTER TABLE `card_category`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `query_bank`
--
ALTER TABLE `query_bank`
  ADD PRIMARY KEY (`query_id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `send_cards`
--
ALTER TABLE `send_cards`
  ADD PRIMARY KEY (`card_key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `card_category`
--
ALTER TABLE `card_category`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `query_bank`
--
ALTER TABLE `query_bank`
  MODIFY `query_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `send_cards`
--
ALTER TABLE `send_cards`
  MODIFY `card_key` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
