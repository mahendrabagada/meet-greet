<html>
	<head>
		<title> Meet & Greet </title>
		<?php include("all_css.html");?>
		<script language="javascript">
		function validate1(frm)
		{
			if(frm.Sname.value=="")
			{
				alert("Sender name can not be blank");
				return false;
				}
			if(frm.Rname.value=="")
			{
				alert("Receiver name can not be blank");
				return false;
				}
			if(frm.Semail.value=="")
			{
				alert("Sender Email can not be blank");
				return false;
				}
			if(frm.Remail.value=="")
			{
				alert("Receiver Email can not be blank");
				return false;
				}
		}	
	</script>
	</head>
	
	<body>
		<div class="jumbotron">
	  <div class="container">
		<h1>Welcome to Meet & Greet</h1>
		
		<p> Here you can Send E-card To Your Friend , Family ,etc . </p>                  
	  </div>
	</div>
		<div class="container">
		<?php include("menu_1.php");?>
		
		<div class="row">
			<div class="col-sm-2 col-md-3 col-lg-12">
			<p>
				<div>
					<h2 align="center">Personalized your E-card</h2>
					<form name="preview" method="post" action="preview.php">
					<?php
						$card_id=$_POST["cardid"];
						print "<input type=hidden name='cardid' value=".$card_id.">";
					?>
					<table class="table table-responsive">
					<tr class="form-group">
						<td><label>Your Name:</label></td>
						<td><input type="text" class="form-control" name="Sname" /></td>
					</tr>
					<tr class="form-group">
						<td><label>Receiver Name:</label></td>
						<td><input type="text" class="form-control" name="Rname"/></td>
					</tr>
					<tr class="form-group">
						<td><label>Your Email-id:</label></td>
						<td><input type="text" class="form-control" name="Semail"/></td>
					</tr>
					<tr class="form-group">
						<td><label>Receiver Email-id:</label></td>
						<td><input type="text" class="form-control" name="Remail" /></td>
					</tr>
					<tr class="form-group">
						<td><label>Header:</label></td>
						<td><input type="text" class="form-control" name="header"/></td>
					</tr>
					<tr class="form-group">
						<td><label>Footer:</label></td>
						<td><input type="text" class="form-control" name="footer" /></td>
					</tr>
					<tr class="form-group">
						<td><label>Message:</label></td>
						<td><textarea rows="4" cols="21" class="form-control" name="message"></textarea></td>
					</tr>
					<tr class="form-group">
						<td colspan="2" align="center">
				<input type="submit" name="prv" class="btn btn-default" value="Preview" id="input" onclick="return validate1(preview);" >
							</td>
					
					</tr>
					</table>
			</form>
				</div>
					
			</p>
			</div>
		
		</div>
		<!-- Start Footer-->
		<?php include("footer.php");?>
		<!-- End Footer-->
		
		</div>
	</body>
</html>

