<?php
	session_start();
	if(!isset($_SESSION['adm']))
	{
		header("location:index.php");
	}
?>
<html>
	<head>
		<title> Meet & Greet </title>
		<link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/bootstrap.css" />
		<link rel="stylesheet" href="../css/menu_s.css" />
		<link rel="stylesheet" href="../css/css2.css" />
		<script src="../js/bootstrap.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/jquery.min.js"></script>
	</head>
	
<body> <?php include("admin_menu.html");?>
		<div class="jumbotron">
	  <div class="container">
		<h1>Welcome to Admin of Meet & Greet</h1>
	  </div>
	</div>
		<div class="container">
		<div class="row">
		<div class="col-sm-8 col-md-8 col-lg-12">
		<p>
			<div class="mn">
				<h2 align="center"><b><u>Welcome Administrator</u></b></h2>
						<br>
						
					This Admin Panel will helps you to handle all the administrative task like Upload Cards, Manage Cards, Add Categories etc.<br />
					This will helps you through out the website handling with menus. Here mange the all things regrarding our site is quite easy with user friendly enviornment.
					<h3><strong>Upload Card:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can upload cards with its Thumbnail, Background, Forground and even Color. Your color must be in RGB format that means Red, Green, Blue it should be within the 0 to 255 for R,G,B like(0,125,233) etc.</p>
					<h3><strong>Card Category:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can add categories for cards. after adding you can view category list as well as Delete it. Bute <b>Note</b>that if you delete any category than category will be deleted but as well as all the cards corresponding this category and which are uploaded all will be deleted simulteneously.</p>
					<h3><strong>Manage Cards:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can view cards with thumbnail, background and forground and even you can search category wise so all things will not messhed up. and you can delete any card if you wish to do so by just click on Delete link.</p>
					<h3><strong>Change Password:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can change your password if you want to change. And every 15 days if you change your password it is better for security purpose.</p> 
					<h3><strong>User:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can display list of all available user and allows you to also delete them.</p> 
					<h3><strong>Logout:</strong></h3>
					<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here you can logout administer site.</p> 
					
			</div>
			</p></div>
			
				</div>
				</div>
		<!-- Start Footer-->
		<?php include("../footer.php");?>
		<!-- End Footer-->
		
		</div>
	</body>
</html>

