

<html>
	<head>
		<title> Meet & Greet </title>
		<?php include("all_css.html");?>
		
	<script language="javascript">
	function validate()
	{
		
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		if(!frm.mail_id.value.match(mailformat))  
		{  
			alert("You have entered an invalid email address!");  
			mail_id.focus();  
			return false; 
			  
		}  
		if(frm.query_type.value=="Select Type")
		{
			alert("Plese Select Appropriate Query Type");
			query_type.focus();
			return false;
		}
		if(frm.msg.value=="")
		{
			alert("State name can not be blank");
			msg.focus();
			return false;
		}		
	}
	
	</script>
		
	</head>
	
	<body> <?php include("menu_1.php");?>
		<div class="jumbotron">
	  <div class="container">
		<h1>Welcome to Meet & Greet</h1>
		
		<p> Here you can Send E-card To Your Friend , Family ,etc . </p>                  
	  </div>
	</div>
		<div class="container">
		<?php include("menu_1.php");?>
		
		<div class="row">
		<div class="col-sm-2 col-md-3 col-lg-8">
		<p>
			<div>
				<form name="frm" action="query.php" method="post" onSubmit="return validate();">
				
				<h2 align="center">Contact </h2>
				
				<table class="table table-responsive">
				<tr>
					<td>Your E-mail ID:-</td>
					<td><input class="form-control"type="text" name="mail_id" autofocus></td>
				</tr>
				<tr>
					<td>Query Type</td>
					<td><select name="query_type" class="form-control"/>
						<option>Select Type</option>
						<option>Request</option>
						<option>Query</option>
						<option>Compliment</option>
						<option>Feedback</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Message</td>
					<td><textarea class="form-control" name="msg" rows="14" cols="15"></textarea></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<input type="submit" name="sub" value="Submit Query" class="btn btn-default"/></td><br/>
				</tr>
				</table>
				</form>
		</div>
					
		</p></div>
		
		</div>
		<!-- Start Footer-->
		<?php include("footer.php");?>
		<!-- End Footer-->
		
		</div>
	</body>
</html>

